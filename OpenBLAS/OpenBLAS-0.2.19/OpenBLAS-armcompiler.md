# Installation instructions

Download and unpack OpenBLAS tarball:
```
$ wget -O openblas-0.2.19.tar.gz https://github.com/xianyi/OpenBLAS/archive/v0.2.19.tar.gz
$ tar -xzf openblas-0.2.19.tar.gz
```
Download and apply Flang patch:
```
$ cd OpenBLAS-0.2.19
$ wget https://github.com/xianyi/OpenBLAS/commit/42bbe74791bfd2b92e7011d54f8b4725413a9fa4.patch
$ patch -p1 < 42bbe74791bfd2b92e7011d54f8b4725413a9fa4.patch
```
Run 'make' to start building process (adjust NUM_THREADS and PREFIX):
```
$ make CC=armclang FC=armflang TARGET=ARMV8 NUM_THREADS=6 USE_THREAD=1 USE_OPENMP=1 PREFIX=$HOME/openblas
```
Run 'make install' to install it in desired location (adjust PREFIX if needed):
```
$ make PREFIX=$HOME/openblas install
```
That's it, enjoy.
