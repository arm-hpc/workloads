------------------------------------------------------
# Configuration
------------------------------------------------------
1. OpenMPI version 2.1.1
2. Arm Compiler version 1.4
3. Tested on TX1 running Ubuntu 16.04

------------------------------------------------------
# Build instructions
------------------------------------------------------
## Build and install directories
```
BENCH_ROOT=~/OpenMPI
BENCH_ARCHIVE=$BENCH_ROOT/Archive
BENCH_BUILD=$BENCH_ROOT/ARMClang
BENCH_INSTALL=~/opt/

```
## Load compiler module
On Ubuntu 16.04, this would be
```
module load Generic-AArch64/Ubuntu/16.04/suites/arm-compiler-for-hpc/1.4
```
## Compiler configuration

```
export CC=armclang
export CXX=armclang++
export FC=armflang
```

## Downloading the packages
```
mkdir -p $BENCH_ARCHIVE
cd $BENCH_ARCHIVE
wget -N https://www.open-mpi.org/software/ompi/v2.1/downloads/openmpi-2.1.1.tar.gz
```

## Extracting the package
```
mkdir $BENCH_BUILD
cd $BENCH_BUILD
tar xzf $BENCH_ARCHIVE/openmpi-2.1.1.tar.gz
```

## Build configuration
```
mkdir build
cd build
../configure --prefix=$BENCH_INSTALL --enable-mpirun-prefix-by-default --enable-debug
```
## Patch output of configure 
This is needed because autoconf and other similar tools do not detect arm compiler properly yet.
```
sed -i -e 's#wl=""#wl="-Wl,"#g' libtool
sed -i -e 's#pic_flag=""#pic_flag=" -fPIC -DPIC"#g' libtool
```

## Build and Install
```
make -j128
make install
```

## Set PATH to ensure mpif90 and other commands use Arm Compiler
```
export PATH=$BENCH_INSTALL/bin:$PATH
```

------------------------------------------------------
#  Run an example Fortran program
------------------------------------------------------

## Create a test file
Create a test file mpihello.f90 with following content
```
! mpihello.f90


program mpihello
  use mpi
  implicit none


   integer :: err, rank, size


   call MPI_Init(ierror = err)
   call MPI_Comm_rank(comm = MPI_COMM_WORLD, rank = rank, ierror = err)
   call MPI_Comm_size(comm = MPI_COMM_WORLD, size = size, ierror = err)
   write (*, 10), rank, size
   call MPI_Finalize(ierror = err)
10 format(' Hello World, rank: ', I5, ' size: ', I5)

end program mpihello

```
## Build and run the test
```
mpif90 -O3 -o mpihello mpihello.f90
mpiexec -np 4 ./mpihello
```

You should see output similar to what is shown below (Note that the ordering may change due to parallelism)
```
 Hello World, rank:     0 size:     4
 Hello World, rank:     2 size:     4
 Hello World, rank:     1 size:     4
 Hello World, rank:     3 size:     4
```

