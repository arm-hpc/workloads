# High Performance Conjugate Gradient Benchmark (HPCG) 

## HPCG (APEX)
HPCG is part of APEX benchmark suite - See http://www.nersc.gov/research-and-development/apex/apex-benchmarks/hpcg/ for more details

## HPCG 3.0 
HPCG - Official repo at  https://github.com/hpcg-benchmark/hpcg/
