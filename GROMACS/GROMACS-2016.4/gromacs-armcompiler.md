------------------------------------------------------
# Configuration
------------------------------------------------------
1. GROMACS version 2016.4
2. armclang version 1.4
3. Tested on TX1 running Ubuntu 16.04  and TX2 running Red Hat 7.3

------------------------------------------------------
# Build instructions
------------------------------------------------------

```
BENCH_ROOT=~/GROMACS
BENCH_ARCHIVE=$BENCH_ROOT/Archive
BENCH_BUILD=$BENCH_ROOT/ARMClang
```

## Compiler configuration

```
export CC=armclang
export CXX=armclang++
export FC=armflang
export OMPI_CC=$CC
export OMPI_CXX=$CXX
export OMPI_FC=$FC
```

## Downloading the packages
```
mkdir -p $BENCH_ARCHIVE
cd $BENCH_ARCHIVE
wget -N http://ftp.gromacs.org/pub/gromacs/gromacs-2016.4.tar.gz
wget -N http://www.prace-ri.eu/UEABS/GROMACS/1.2/GROMACS_TestCaseA.tar.gz
```

## Extracting the package
```
mkdir $BENCH_BUILD
cd $BENCH_BUILD
tar xzf $BENCH_ARCHIVE/gromacs-2016.4.tar.gz
```

## Build configuration
```
mkdir build
cd build
cmake \
        -DCMAKE_INSTALL_PREFIX=$BENCH_BUILD/gromacs/2016 \
        -DBUILD_SHARED_LIBS=off \
        -DBUILD_TESTING=on \
        -DREGRESSIONTEST_DOWNLOAD=on \
        -DCMAKE_C_COMPILER=`which mpicc` \
        -DCMAKE_CXX_COMPILER=`which mpicxx` \
        -DGMX_BUILD_OWN_FFTW=on \
        -DGMX_SIMD=ARM_NEON_ASIMD \
        -DGMX_DOUBLE=off \
        -DGMX_EXTERNAL_BLAS=off \
        -DGMX_EXTERNAL_LAPACK=off \
        -DGMX_FFT_LIBRARY=fftw3 \
        -DGMX_GPU=off \
        -DGMX_MPI=on \
        -DGMX_OPENMP=on \
        -DGMX_X11=off \
        ../gromacs-2016.4
```

## Build and Install
```
make -j128
make install
```

## Build tests 
```
make -i check
```

------------------------------------------------------
#  Run a simple benchmark (taking 10s of minutes).
------------------------------------------------------

## Download the benchmark
```
mkdir -p $BENCH_ROOT/Tests/GROMACS_TestCaseA
cd $BENCH_ROOT/Tests/GROMACS_TestCaseA
tar xf $BENCH_ARCHIVE/GROMACS_TestCaseA.tar.gz
```
## Run the benchmark
```
. $BENCH_BUILD/gromacs/2016/bin/GMXRC.bash
OMP_NUM_THREADS=1 mpirun -np 40 gmx_mpi mdrun -s ion_channel.tpr -maxh 0.50 -resethway -noconfout -nsteps 10000 -pin on -npme 0 -g logile
```
